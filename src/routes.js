
import MoviesPage from './components/movies-page/moviesPage';
import ErrorPage from './components/error-page/errorPage';

const routes = [
  {
    path: '/',
    exact: true,
    component: MoviesPage
  },
  {
    path: '/search',
    component: MoviesPage
  },
  {
    path: '/film/:id',
    component: MoviesPage
  },
  {
    path: '*',
    component: ErrorPage
  },
];

export default routes;
