import { render } from '@testing-library/react';
import App from './App';

describe('App Module', () => {
  test('renders learn react link', () => {
    const { asFragment } = render(<App />);
    
    expect(asFragment()).toMatchSnapshot();
  });
})
