
import 'isomorphic-fetch';
import 'babel-polyfill';
import React from 'react';
import { Provider } from 'react-redux';

import {
  Switch,
  Route
} from "react-router-dom";
import MoviesPage from "./components/movies-page/moviesPage";
import ErrorPage from './components/error-page/errorPage';
import { hot } from 'react-hot-loader';

function App({Router, location, context, store}) {

  return (
    <Provider store={store}>
      <Router location={location} context={context}>
        <Switch>
          <Route exact path="/" component={ MoviesPage }/>
          <Route path="/search" component={ MoviesPage }/>
          <Route path="/film/:id" component={ MoviesPage }/>
          <Route path="*" component={ ErrorPage }/>
        </Switch>
      </Router>
    </Provider>
  );
}  

export default hot(module)(App);
