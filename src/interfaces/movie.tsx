export interface MovieType {
  id: string,
  title: string,
  tagline: string,
  vote_average: number
  vote_count: number,
  release_date: string,
  poster_path: string,
  overview: string,
  budget: number,
  revenue: number,
  genres: Array<string>,
  runtime: number,
  
}

export interface MovieInputType {
  title: string,
  tagline: string,
  vote_average: number
  vote_count: number,
  release_date: string,
  poster_path: string,
  overview: string,
  budget: number,
  revenue: number,
  genres: Array<string>,
  runtime: number,
  
}

export interface Movies {
  totalAmount: number,
  data: Array<MovieType>,
  offset: number,
  limit: number
}
export interface MovieData {
  data: MovieType,
}

export interface MovieReducer {
  movies: Movies,
  movie: MovieData,
  loading: boolean,
  errorMessage: string,
}
export interface StateType {
  movieReducer: MovieReducer 
}