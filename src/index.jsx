//import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.bundle.min'

import React from 'react';
import ReactDOM from "react-dom";
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import store from '../src/store/store';
import App from './App';

const store1 = store(window.PRELOADED_STATE);

/*const app = (
  <App
    Router={BrowserRouter}
    store={store1}
  />
);*/
ReactDOM.hydrate(
  <React.StrictMode>
    <App 
      Router={BrowserRouter}
      store={store1}
    />
  </React.StrictMode>,
  document.getElementById('root')
);

//hydrate(app, document.getElementById('root'));
