import axios from "axios";
import { 
  fetchMoviesFail, 
  fetchMoviesRequest, 
  fetchMoviesSuccess,
  fetchMovieFail, 
  fetchMovieRequest, 
  fetchMovieSuccess,
  createMovieSuccess,
  createMovieFail, 
  createMovieRequest,
  updateMovieRequest,
  updateMovieSuccess,
  updateMovieFail,
  deleteMovieRequest,
  deleteMovieSuccess,
  deleteMovieFail
} from "./movieActions"

export const fetchMoviesDispatch = (filters="") => {
  return (dispatch) => {
    dispatch(fetchMoviesRequest());
    console.log("filters:", filters);
    axios.get("http://localhost:4000/movies"+filters)
    .then(response => {
      dispatch(fetchMoviesSuccess(response.data))
    })
    .catch(error => {
      dispatch(fetchMoviesFail(error.message))
    })
  }
}

export const fetchMovieDispatch = (id) => {
  return (dispatch) => {
    dispatch(fetchMovieRequest());
    axios.get("http://localhost:4000/movies/"+id)
    .then(response => {
      dispatch(fetchMovieSuccess(response))
    })
    .catch(error => {
      dispatch(fetchMovieFail(error.message))
    })
  }
}

export const cretaeMovieDispatch = (movie) => {
  
  return (dispatch) => {
    dispatch(createMovieRequest());
    axios.post('http://localhost:4000/movies', movie)
    .then(response => {
      dispatch(createMovieSuccess(response.data));
    })
    .catch(error => {
      console.log(error);
      dispatch(createMovieFail(error.messages))
    })
  }
}

export const updateMovieDispatch = (movie) => {
  
  return (dispatch) => {
    dispatch(updateMovieRequest());
    axios.put('http://localhost:4000/movies', movie)
    .then(response => {
      dispatch(updateMovieSuccess(response.data));
      dispatch(fetchMoviesDispatch());
    })
    .catch(error => {
      console.log("Error update: ", error)
      dispatch(updateMovieFail(error.message))
    })
  }
}

export const deleteMovieDispatch = (id) => {
  
  return (dispatch) => {
    dispatch(deleteMovieRequest());
    axios.delete('http://localhost:4000/movies/'+id)
    .then(() => {
      dispatch(deleteMovieSuccess());
      dispatch(fetchMoviesDispatch());
    })
    .catch(error => {
      dispatch(deleteMovieFail(error.message))
    })
  }
}