import ACTIONS from "./movieTypes";

export const fetchMoviesRequest = () => {
  return {
    type: ACTIONS.FETCH_MOVIES_REQUEST,
  }
}

export const fetchMoviesSuccess = (movies) => {
  return {
    type: ACTIONS.FETCH_MOVIES_SUCCESS,
    payload: movies
  }
}

export const fetchMoviesFail = (errorMessage) => {
  return {
    type: ACTIONS.FETCH_MOVIES_FAIL,
    payload: errorMessage
  }
}

export const fetchMovieRequest = () => {
  return {
    type: ACTIONS.FETCH_MOVIE_REQUEST,
  }
}

export const fetchMovieSuccess = (movie) => {
  return {
    type: ACTIONS.FETCH_MOVIE_SUCCESS,
    payload: movie
  }
}

export const fetchMovieFail = (errorMessage) => {
  return {
    type: ACTIONS.FETCH_MOVIE_FAIL,
    payload: errorMessage
  }
}

export const createMovieRequest = () => {
  return {
    type: ACTIONS.CREATE_MOVIE_REQUEST,
    payload: ""
  }
}

export const createMovieSuccess = (movie) => {
  return {
    type: ACTIONS.CREATE_MOVIE_SUCCESS,
    payload: movie
  }
}

export const createMovieFail = (errorMessage) => {
  return {
    type: ACTIONS.CREATE_MOVIE_FAIL,
    payload: errorMessage
  }
}

export const updateMovieRequest = () => {
  return {
    type: ACTIONS.UPDATE_MOVIE_REQUEST,
    payload: ""
  }
}

export const updateMovieSuccess = (movie) => {
  return {
    type: ACTIONS.UPDATE_MOVIE_SUCCESS,
    payload: movie
  }
}

export const updateMovieFail = (errorMessage) => {
  return {
    type: ACTIONS.UPDATE_MOVIE_FAIL,
    payload: errorMessage
  }
}

export const deleteMovieRequest = () => {
  return {
    type: ACTIONS.DELETE_MOVIE_REQUEST,
  }
}

export const deleteMovieSuccess = (movie) => {
  return {
    type: ACTIONS.UPDATE_MOVIE_SUCCESS,
  }
}

export const deleteMovieFail = (errorMessage) => {
  return {
    type: ACTIONS.UPDATE_MOVIE_FAIL,
    payload: errorMessage
  }
}