import { initialState } from "./initialState";
import ACTIONS from "./movieTypes";

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case ACTIONS.FETCH_MOVIES_REQUEST:
      console.log("actions fetching .... ");
      return {
        ...state,
        loading: true,
        errorMessage:""
      }
    case ACTIONS.FETCH_MOVIES_SUCCESS:
      return {
        ...state,
        movies: action.payload,
        loading: false,
        errorMessage:""
      }
    case ACTIONS.FETCH_MOVIES_FAIL:
      return{
        ...state,
        movies: [],
        loading: false,
        errorMessage: action.payload
      }
      case ACTIONS.FETCH_MOVIE_REQUEST:
        return {
          ...state,
          loading: true,
          errorMessage:""
        }
      case ACTIONS.FETCH_MOVIE_SUCCESS:
        return {
          ...state,
          movie: action.payload,
          loading: false,
          errorMessage:""
        }
      case ACTIONS.FETCH_MOVIE_FAIL:
        return{
          ...state,
          movie: {},
          loading: false,
          errorMessage: action.payload
        }
    case ACTIONS.CREATE_MOVIE_REQUEST:
      return {
        ...state,
        movie: {},
        loading: true,
        errorMessage:""
      }
    case ACTIONS.CREATE_MOVIE_SUCCESS:
      return {
        ...state,
        movie: action.payload,
        loading: false,
        errorMessage:""
      }
    case ACTIONS.CREATE_MOVIE_FAIL:
      return{
        ...state,
        movie: {},
        loading: false,
        errorMessage: action.payload
      }
      case ACTIONS.UPDATE_MOVIE_REQUEST:
        return {
          ...state,
          movie: {},
          loading: true,
          errorMessage:""
        }
      case ACTIONS.UPDATE_MOVIE_SUCCESS:
        return {
          ...state,
          movie: action.payload,
          loading: false,
          errorMessage:""
        }
      case ACTIONS.UPDATE_MOVIE_FAIL:
        return{
          ...state,
          movie: {},
          loading: false,
          errorMessage: action.payload
        }
        case ACTIONS.DELETE_MOVIE_REQUEST:
          return {
            ...state,
            loading: true,
            errorMessage:""
          }
        case ACTIONS.DELETE_MOVIE_SUCCESS:
          return {
            ...state,
            loading: false,
            errorMessage:""
          }
        case ACTIONS.DELETE_MOVIE_FAIL:
          return{
            ...state,
            loading: false,
            errorMessage: action.payload
          }
    default:
      return state;
  }
}

export default reducer;