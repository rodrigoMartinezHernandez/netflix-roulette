export const initialState = {
  movies: [],
  movie: {},
  loading: false,
  errorMessage: "",
};