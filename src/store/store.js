import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducer from './rootReducer';



const store = createStore(reducer, composeWithDevTools(
  applyMiddleware(logger, thunk),
));

export default store;
