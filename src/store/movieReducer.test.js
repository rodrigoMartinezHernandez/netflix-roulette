import { MovieMock } from "../components/mockData";
import { initialState } from "./initialState";
import movieReducer from "./movieReducer";
import ACTIONS from "./movieTypes";

describe('get movie reducer', () => {
  it('returns the initial state', () => {
    expect(movieReducer(undefined, {})).toEqual(initialState);
  });

  it('handles fetch movie request', () => {
    expect(movieReducer(initialState, {type: ACTIONS.FETCH_MOVIE_REQUEST})).toEqual({
      ...initialState,
      loading: true
    })
  })

  it('handles fetch movie failure', () => {
    expect(movieReducer(initialState, {type: ACTIONS.FETCH_MOVIES_FAIL, payload:'error'})).toEqual({
      ...initialState,
      errorMessage: 'error'
    })
  })

  it('handles fetch movie success', () => {
    expect(movieReducer(initialState, {type: ACTIONS.FETCH_MOVIE_SUCCESS, payload:MovieMock})).toEqual({
      ...initialState,
      movie: MovieMock
    })
  })
});
