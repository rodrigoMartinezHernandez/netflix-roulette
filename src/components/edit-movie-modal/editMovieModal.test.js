import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
//import store from '../../store/store';
import { Provider } from "react-redux";
import { act } from 'react-dom/test-utils';
import ModalContext from "../modalContext";
import { shallow } from 'enzyme';

import EditMovieModal from './editMovieModal';
import { MovieMock } from '../mockData';
import userEvent from '@testing-library/user-event';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../store/initialState';
const mockStore = configureMockStore();
function renderEditMovieModal() {
  const initialState = {
    movieReducer: {
      loading: false
    }
  } 
  const store = mockStore(initialState);
  act(() => {
    render(
      <Provider store={store}>
          <EditMovieModal movie={MovieMock}/>
      </Provider>
    );
  })
}
describe('EditMovieModal', () => {
  it('renders fire title change event', () => {
    renderEditMovieModal();
    act(() => {
      fireEvent.change(screen.getByPlaceholderText('Title here'), {
        target: { value: 'Star Wars' }
      });
    });

    expect(screen.getByDisplayValue('Star Wars')).toBeInTheDocument();
  });

  xit('renders url change event', () => {
    renderEditMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Movie URL here'), {
       target: { value: 'http://starwars' }
     });
    });
 
     expect(screen.getByDisplayValue('http://starwars')).toBeInTheDocument();
   });

   it('renders genres change event', () => {
    renderEditMovieModal();
    act(() => {
     fireEvent.change(screen.getByTestId ('genres'), {
       target: { value: 'Comedy' }
     });
    });
 
     expect(screen.getByDisplayValue('COMEDY')).toBeInTheDocument();
   });

   it('renders overview change event', () => {
    renderEditMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Overview here'), {
       target: { value: 'Movie description' }
     });
    });
 
     expect(screen.getByDisplayValue('Movie description')).toBeInTheDocument();
   });

   it('renders runtime change event', async () => {
    renderEditMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Runtime here'), {
       target: { value: '120' }
     });
    });
 
     expect(screen.getByDisplayValue('120')).toBeInTheDocument();
   });

   it('launch submitMovie event', async () => {
      const initialState = {
        movieReducer: {
          loading: false
        }
      } 
      const store = mockStore(initialState);
      const updateMovie = jest.fn();
      render(
        <Provider store={store}>
            <EditMovieModal updateMovie={updateMovie} movie={MovieMock}/>
        </Provider>
      );
      await fireEvent.click(screen.getByText(/SAVE/i));
      screen.debug();
      expect(updateMovie).toHaveBeenCalledTimes(0);
   });

   it('launch closeEditMovieModal event', () => {
    const initialState = {
      movieReducer: {
        loading: false
      }
    } 
    const store = mockStore(initialState);
    const closeEditMovieModal = jest.fn();
    act(() => {
      render(
        <Provider store={store}>
          <ModalContext.Provider value={{closeEditMovieModal}}>
            <EditMovieModal movie={MovieMock}/>
          </ModalContext.Provider>
        </Provider>
      );
    })
    fireEvent.click(screen.getByTestId('closeEditModal'),{});
    expect(closeEditMovieModal).toHaveBeenCalledTimes(1);
   });

})