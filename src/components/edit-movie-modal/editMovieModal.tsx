import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext } from "react";
import { connect } from "react-redux";
import { MovieType, StateType } from "../../interfaces/movie";
import { updateMovieDispatch } from "../../store/movieDispatchers";
import ModalContext from "../modalContext";
import './editMovieModal.css';
import * as Yup from 'yup';
import { useFormik } from "formik";

const EditMovieModal = ({ movie, loading, updateMovie }: any) => {
  const context = useContext(ModalContext);

  const MovieSchema =Yup.object().shape({
    title:Yup.string()
    .min(2, 'Too shoort!')
    .required('Required'),
    date:Yup.string()
    .required('Required'),
    posterPath:Yup.string()
    .required('Required'),
    genres:Yup.string()
    .required('Required'),
    overview:Yup.string()
    .required('Required'),
    runtime:Yup.number()
    .min(0)
    .required('Required')
  })
  const submitMovie = (values: any) => {
    sendSuccessAlert();
    updateMovie(getMovieToUpdate(values));

    context.closeEditMovieModal();
  };

  const getMovieToUpdate = (values:any) => {
    return {...movie,
      title: values.title,
      release_date: values.date,
      genres: [values.genres],
      overview: values.overview,
      runtime: values.runtime,
      poster_path: values.posterPath,
    };
  }

  const sendSuccessAlert = () => {
    if(isLoaded()){
      alert("Movie successfull updated");
    }
  }

  const isLoaded = () => {
    return !loading;
  }

  const formik = useFormik({
    initialValues: {
      title: movie.title,
      date: movie.release_date, 
      genres: movie.genres[0], 
      posterPath: movie.poster_path, 
      overview: movie.overview, 
      runtime: movie.runtime
    },
    onSubmit: submitMovie,
    validationSchema: MovieSchema
  });
  return(
    <>
      <div className="modal-container">
        <h1 id="modal-title">EDIT MOVIE</h1>
        <button className="modal-close" data-testid="closeEditModal" onClick={()=>context.closeEditMovieModal()}><FontAwesomeIcon icon={faTimes}/></button>
        <form action="" id="add-movie-form" onSubmit={formik.handleSubmit}>
          <label htmlFor="movie-id">MOVIE ID</label>
          <p id="movie-id" >{movie.id}</p>

          <label htmlFor="title">TITLE</label>
          <input type="text" name="title" placeholder="Title here" 
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.title}
          ></input>
          {formik.errors.title && formik.touched.title ? (
            <div>{formik.errors.title}</div>
          ) : null}

          <label htmlFor="date">RELEASE DATE</label>
          <input type="date" name="date"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.date}
          ></input>
          {formik.errors.date && formik.touched.date ? (
            <div>{formik.errors.date}</div>
          ) : null}

          <label htmlFor="posterPath">MOVIE URL</label>
          <input type="text" name="posterPAth" placeholder="Movie URL here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.posterPath}
          ></input>
          {formik.errors.posterPath && formik.touched.posterPath ? (
            <div>{formik.errors.posterPath}</div>
          ) : null}

          <label htmlFor="genres">GENRE</label>
          <select name="genres"
            onChange={formik.handleChange} data-testid="genres" onBlur={formik.handleBlur} value={formik.values.genres}
          >
            <option value="">Select Genre</option>
            <option value="Documentary">DOCUMENTARY</option>
            <option value="Comedy">COMEDY</option>
            <option value="Horror">HORROR</option>
            <option value="Crime">CRIME</option>
          </select>
          {formik.errors.genres && formik.touched.genres ? (
            <div>{formik.errors.genres}</div>
          ) : null}

          <label htmlFor="overview">OVERVIEW</label>
          <input type="text" name="overview" placeholder="Overview here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.overview}
          ></input>
          {formik.errors.overview && formik.touched.overview ? (
            <div>{formik.errors.overview}</div>
          ) : null}

          <label htmlFor="runtime">RUNTIME</label>
          <input type="number" name="runtime" placeholder="Runtime here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.runtime}
          ></input>
          {formik.errors.runtime && formik.touched.runtime ? (
            <div>{formik.errors.runtime}</div>
          ) : null}

          <div className="button-container">
            <button className="reset-button">RESET</button>
            <button className="submit-button" type="submit">SAVE</button>
          </div>
          
        </form>

      </div>
      <div className="background-modal">

      </div>
    </>
  )
}
const mapStateToProps = (state: StateType) =>({ 
  loading: state.movieReducer.loading
});


const mapDispatchToProps = {
    updateMovie: (movie: MovieType) => updateMovieDispatch(movie)
}
export default connect(mapStateToProps, mapDispatchToProps)(EditMovieModal);