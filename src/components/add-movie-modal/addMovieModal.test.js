import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import store from '../../store/store';
import { Provider } from "react-redux";
import { act } from 'react-dom/test-utils';
import ModalContext from "../modalContext";
import { shallow } from 'enzyme';

import AddMovieModal from './addMovieModal';
import { MovieMock } from '../mockData';
import userEvent from '@testing-library/user-event';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../store/initialState';
const mockStore = configureMockStore();
function renderAddMovieModal() {
  act(() => {
    render(
      <Provider store={store}>
          <AddMovieModal />
      </Provider>
    );
  })
}
describe('AddMovieModal', () => {
  it('renders fire title change event', () => {
    renderAddMovieModal();
    act(() => {
      fireEvent.change(screen.getByPlaceholderText('Title here'), {
        target: { value: 'Star Wars' }
      });
    });

    expect(screen.getByDisplayValue('Star Wars')).toBeInTheDocument();
  });

  it('renders url change event', () => {
    renderAddMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Movie URL here'), {
       target: { value: 'http://starwars' }
     });
    });
 
     expect(screen.getByDisplayValue('http://starwars')).toBeInTheDocument();
   });

   it('renders genres change event', () => {
    renderAddMovieModal();
    act(() => {
     fireEvent.change(screen.getByTestId ('genres'), {
       target: { value: 'Comedy' }
     });
    });
 
     expect(screen.getByDisplayValue('COMEDY')).toBeInTheDocument();
   });

   it('renders overview change event', () => {
    renderAddMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Overview here'), {
       target: { value: 'Movie description' }
     });
    });
 
     expect(screen.getByDisplayValue('Movie description')).toBeInTheDocument();
   });

   it('renders runtime change event', async () => {
    renderAddMovieModal();
    act(() => {
     fireEvent.change(screen.getByPlaceholderText('Runtime here'), {
       target: { value: '120' }
     });
    });
 
     expect(screen.getByDisplayValue('120')).toBeInTheDocument();
   });

   it('launch submitMovie event', async () => {
     const createMovie = jest.fn();
      render(
        <Provider store={store}>
            <AddMovieModal createMovie={createMovie}/>
        </Provider>
      );
      await fireEvent.click(screen.getByText(/SUBMIT/i));
      screen.debug();
      expect(createMovie).toHaveBeenCalledTimes(0);
   });

   it('launch closeAddMovieModal event', () => {
    const closeAddMovieModal = jest.fn();
    act(() => {
      render(
        <Provider store={store}>
          <ModalContext.Provider value={{closeAddMovieModal}}>
            <AddMovieModal />
          </ModalContext.Provider>
        </Provider>
      );
    })
    fireEvent.click(screen.getByTestId('closeModal'),{});
    expect(closeAddMovieModal).toHaveBeenCalledTimes(1);
   });

})