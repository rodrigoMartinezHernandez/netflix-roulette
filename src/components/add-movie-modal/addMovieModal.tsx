import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Formik, useFormik } from "formik";
import React, { useContext, useState } from "react";
import { connect } from "react-redux";
import { MovieInputType, MovieType, StateType } from "../../interfaces/movie";
import { cretaeMovieDispatch } from "../../store/movieDispatchers";
import ModalContext from "../modalContext";
import './addMovieModal.css'
import * as Yup from 'yup';

const initialMovie:MovieInputType = {
  title: "",
  release_date: "2021-03-01",
  tagline: "Here's to the fools who dream.",
  genres: [],
  overview: "",
  runtime: 0,
  poster_path: "",
  vote_average: 10,
  vote_count: 1000,
  budget: 1000,
  revenue:1000,
}

const AddMovieModal = ({createMovie, loading}: any) => {
  const context = useContext(ModalContext);
  

  const MovieSchema =Yup.object().shape({
    title:Yup.string()
    .min(2, 'Too shoort!')
    .required('Required'),
    date:Yup.string()
    .required('Required'),
    posterPath:Yup.string()
    .required('Required'),
    genres:Yup.string()
    .required('Required'),
    overview:Yup.string()
    .required('Required'),
    runtime:Yup.number()
    .min(0)
    .required('Required')
  })

  const submitMovie = (values: any) => {
    createMovie(getMovieToSubmit(values));
    sendSuccessAlert();

    context.closeAddMovieModal();
  };

  const getMovieToSubmit = (values:any) => {
    return {...initialMovie,
      title: values.title,
      release_date: values.date,
      genres: [values.genres],
      overview: values.overview,
      runtime: values.runtime,
      poster_path: values.posterPath,
    };
  }

  const sendSuccessAlert = () => {
    if(isLoaded()){
      alert("Movie successfull created");
    }
  }

  const isLoaded = () => {
    return !loading;
  }
  const formik = useFormik({
    initialValues: {
      title:"", 
      date: "", 
      genres:"" , 
      posterPath:"", 
      overview:"", 
      runtime:0
    },
    onSubmit: submitMovie,
    validationSchema: MovieSchema
  });

  return(
    <>
      <div className="modal-container">
        <h1 id="modal-title">ADD MOVIE</h1>
        <button className="modal-close" data-testid="closeModal" onClick={()=>context.closeAddMovieModal()}><FontAwesomeIcon icon={faTimes}/></button>
        
        <form action="" id="add-movie-form" data-testid="formAddMovie" onSubmit={formik.handleSubmit}>
          <label htmlFor="title">TITLE</label>
          <input type="text"  name="title" placeholder="Title here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.title}
          ></input>
          {formik.errors.title && formik.touched.title ? (
            <div>{formik.errors.title}</div>
          ) : null}

          <label htmlFor="date">RELEASE DATE</label>
          <input type="date"  name="date" placeholder="Select Date"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.date}
          ></input>
          {formik.errors.date && formik.touched.date ? (
            <div>{formik.errors.date}</div>
          ) : null}

          <label htmlFor="movie-url">MOVIE URL</label>
          <input type="text" name="posterPath" placeholder="Movie URL here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.posterPath}
          ></input>
          {formik.errors.posterPath && formik.touched.posterPath ? (
            <div>{formik.errors.posterPath}</div>
          ) : null}
          
          <label htmlFor="genres">GENRE</label>
          <select name="genres" data-testid="genres"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.genres}
          >
            <option value="">Select Genre</option>
            <option value="Documentary">DOCUMENTARY</option>
            <option value="Comedy">COMEDY</option>
            <option value="Horror">HORROR</option>
            <option value="Crime">CRIME</option>
          </select>
          {formik.errors.genres && formik.touched.genres ? (
            <div>{formik.errors.genres}</div>
          ) : null}

          <label htmlFor="overview">OVERVIEW</label>
          <input type="text" name="overview" placeholder="Overview here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.overview}
          ></input>
          {formik.errors.overview && formik.touched.overview ? (
            <div>{formik.errors.overview}</div>
          ) : null}

          <label htmlFor="runtime">RUNTIME</label>
          <input type="number" name="runtime" placeholder="Runtime here"
            onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.runtime}
          ></input>
          {formik.errors.runtime && formik.touched.runtime ? (
            <div>{formik.errors.runtime}</div>
          ) : null}
            
          <div className="button-container">
            <button className="reset-button" >RESET</button>
            <button className="submit-button" type="submit">SUBMIT</button>
          </div>
          
        </form>
        

      </div>
      <div className="background-modal">

      </div>
    </>
  )
}
const mapStateToProps = (state: StateType) =>({ 
  loading: state.movieReducer.loading
});


const mapDispatchToProps = {
    createMovie: (movie: MovieType) => cretaeMovieDispatch(movie)
}
export default connect(mapStateToProps, mapDispatchToProps)(AddMovieModal);