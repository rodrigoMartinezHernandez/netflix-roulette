import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { StateType } from "../../interfaces/movie";
import { fetchMovieDispatch } from "../../store/movieDispatchers";
import { Link } from "react-router-dom";
import "./movieDetails.css";

const MovieDetails = ({ movieId, fetchMovie, movie }: any) => {
  const [imageUrl, setImageUrl] = useState("");
  useEffect(() => {
    fetchMovie(movieId);
  }, [fetchMovie, movieId])

  const isLodadedMovie = useCallback(() => {
    console.log("imageUrl:", imageUrl);
    return !!movie?.title && movie !== undefined && !!imageUrl
  }, [movie, imageUrl])

  useEffect(() => {
    if(!!movie?.poster_path) {
      axios.get(movie.poster_path).then(() => {
        setImageUrl(movie.poster_path);
      }).catch(() => {
        setImageUrl("images/not-found.png");
      })
    }
  }, [movie])

  return(
    <>
      <div className="col-6" id="btn-back-search">
        <Link
        className="btn-back-search-link"
          to="/"
        >
          <FontAwesomeIcon icon={faSearch}/>
        </Link>
      </div>
      {
        isLodadedMovie()
        ?
          <>
            <div className="col-3 detail-image">
              <img src={imageUrl} alt={movie.title}/>
            </div>
            <div className="col-9" id="detail-container">
              <div className="detail-title">
                <h1>{movie.title}</h1>
                <h1 id="detail-rating">{movie.vote_average}</h1>
              </div>
              <div className="details">
                <p>{movie.genres.join(',')}</p>
                <div className="detail-time-year">
                  <p>{movie.release_date} <span>{movie.runtime} min</span></p>
                </div>
                <p>{movie.overview}</p>
              </div>
              
            </div>
          </>
        :<span>loading</span>
      }
    </>
  )
}

const mapStateToProps = (state: StateType) =>({ 
  movie: state.movieReducer.movie.data,
  loading: state.movieReducer.loading,
});


const mapDispatchToProps = {
    fetchMovie: (id: string) => fetchMovieDispatch(id)
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetails);