import './noMoviesFound.css';

const NoMoviesFound = () => {
  return(
    <div className="no-movies-found-container">
      <h1>No Movie Found</h1>
    </div>
  );
}

export default NoMoviesFound;