import React from 'react';
import { useState, useEffect } from "react";
import './moviesPage.css';
import Header from '../header/Header';
import MoviesList from '../movies-list/moviesList';
import Footer from '../footer/footer';
import ErrorBoundary from '../error-boundary/errorBoundary';

import HeaderHero from "../header-hero/headerHero";
import MovieDetails from "../movie-details/movieDetails";
import AddMovieModal from "../add-movie-modal/addMovieModal";
import EditMovieModal from "../edit-movie-modal/editMovieModal";
import DeleteMovieModal from "../delete-movie-modal/deleteMovieModal";

import ModalContext from "../modalContext";
import { Provider } from "react-redux";

import store from '../../store/store';
import { useParams } from "react-router-dom";

function MoviesPage() {
  const [movieDetailsToggle, setMovieDetailsToggle] = useState(false);
  const [addMovieModalToggle, setAddMovieModalToggle] = useState(false);
  const [editMovieModalToggle, setEditMovieModalToggle] = useState(false);
  const [deleteMovieModalToggle, setDeleteMovieModalToggle] = useState(false);
  const [movieToUpdate, setMovieToUpdate] = useState();
  const [movieIdToDelete, setMovieIdToDelete] = useState();

  let { id } = useParams();
  console.log("Id: ", id);
  useEffect(() => {
    console.log("Id: ", id);
    if(!!id){
      setMovieDetailsToggle(true);
    } else {
      setMovieDetailsToggle(false);
    }
  }, [id])

  const openAddMovieModal = () => {
    setAddMovieModalToggle(true);
  }

  const closeAddMovieModal = () => {
    setAddMovieModalToggle(false);
  }
  const openEditMovieModal = (movie) => {
    setMovieToUpdate(movie);
    setEditMovieModalToggle(true);
  }

  const closeEditMovieModal = () => {
    setEditMovieModalToggle(false);
  }
  const openDeleteMovieModal = (id) => {
    setMovieIdToDelete(id);
    setDeleteMovieModalToggle(true);
  }

  const closeDeleteMovieModal = () => {
    setDeleteMovieModalToggle(false);
  }

  return (
      <ModalContext.Provider value={{
        closeAddMovieModal,
        closeDeleteMovieModal,
        closeEditMovieModal,
        openEditMovieModal,
        openDeleteMovieModal
      }}>
        
        <Header>
          {
            movieDetailsToggle
            ?<MovieDetails  movieId={id} ></MovieDetails>
            :<HeaderHero openAddMovieModal={()=>openAddMovieModal()} ></HeaderHero>
          }
        </Header>
        <ErrorBoundary>
          <MoviesList
          ></MoviesList>
        </ErrorBoundary>
        <Footer></Footer>

        {
          addMovieModalToggle
          ?<AddMovieModal></AddMovieModal>
          :<></>
        }
        {
          editMovieModalToggle
            ? <EditMovieModal movie={movieToUpdate}></EditMovieModal>
            : <></>
        }
        {
          deleteMovieModalToggle
            ? <DeleteMovieModal id={movieIdToDelete}></DeleteMovieModal>
            : <></>
        }
      </ModalContext.Provider>
  );
}

export default MoviesPage;
