import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext } from "react";
import { connect } from "react-redux";
import { StateType } from "../../interfaces/movie";
import { deleteMovieDispatch } from "../../store/movieDispatchers";
import ModalContext from "../modalContext";
import './deleteMovieModal.css';

const DeleteMovieModal = ({id, loading, deleteMovie}: any) => {
  const handleDeleteMovie = (event: any) => {
    event.preventDefault();
    deleteMovie(id);
    if(!loading){
      alert("Movie successfull deleted");
    }
    context.closeDeleteMovieModal()
  }
  const context = useContext(ModalContext);
  return(
    <>
      <div className="modal-container">
        <h1 id="modal-title">DELETE MOVIE</h1>
        <button className="modal-close" data-testid="closeDeleteModal" onClick={()=>context.closeDeleteMovieModal()}><FontAwesomeIcon icon={faTimes}/></button>
        <p className="text-modal" >Are you sure you want to delete this movie?</p>
        <form action="" id="add-movie-form">
          <div className="button-container">
            <button className="submit-button" onClick={(e)=>handleDeleteMovie(e)}>CONFIRM</button>
          </div>
          
        </form>

      </div>
      <div className="bacground-modal">

      </div>
    </>
  )
}
const mapStateToProps = (state: StateType) =>({ 
  loading: state.movieReducer.loading
});


const mapDispatchToProps = {
    deleteMovie: (id: string) => deleteMovieDispatch(id)
}
export default connect(mapStateToProps, mapDispatchToProps)(DeleteMovieModal);