import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import store from '../../store/store';
import { Provider } from "react-redux";
import { act } from 'react-dom/test-utils';
import ModalContext from "../modalContext";

import DeleteMovieModal from './deleteMovieModal';
describe('DeleteMovieModal', () => {
   it('launch submitMovie event', async () => {
     const deleteMovie = jest.fn();
     const jsdomAlert = window.alert;
  window.alert = () => {};
      render(
        <Provider store={store}>
            <DeleteMovieModal deleteMovie={deleteMovie}/>
        </Provider>
      );
      await fireEvent.click(screen.getByText(/CONFIRM/i));
      screen.debug();
      expect(deleteMovie).toHaveBeenCalledTimes(0);
      window.alert = jsdomAlert; 
   });

   it('launch closeAddMovieModal event', () => {
    const closeDeleteMovieModal = jest.fn();
    act(() => {
      render(
        <Provider store={store}>
          <ModalContext.Provider value={{closeDeleteMovieModal}}>
            <DeleteMovieModal />
          </ModalContext.Provider>
        </Provider>
      );
    })
    fireEvent.click(screen.getByTestId('closeDeleteModal'),{});
    expect(closeDeleteMovieModal).toHaveBeenCalledTimes(1);
   });

})