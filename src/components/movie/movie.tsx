import { faEllipsisV, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { MovieType } from "../../interfaces/movie";
import ModalContext from "../modalContext";
import { Link } from "react-router-dom";
import './movie.css';

type MovieProps = {
  movie: MovieType,

}
const Movie = ({ movie }:MovieProps) => {
  const [dotMenuToggle, setDotMenuToggle] = React.useState(true);
  const [imageUrl, setImageUrl] = useState("");

  const haendleDotMenu = useCallback(() => {
    setDotMenuToggle(!dotMenuToggle);
  }, [dotMenuToggle]);
  
  useEffect(() => {
    axios.get(movie.poster_path).then((response) => {
        setImageUrl(movie.poster_path);
    }).catch((error) => {
        setImageUrl("images/not-found.png");
    })
  }, [movie])
  const context = useContext(ModalContext);
  return (
    <div className="col-4" id="img-container">
      {
        dotMenuToggle 
          ?<button id="dots-icon" onClick={()=>haendleDotMenu()}><FontAwesomeIcon icon={faEllipsisV}/></button>
          :<nav id="nav-movie" >
            <ul id="ul-movie">
              <li id="li-close"><button onClick={()=>haendleDotMenu()}><FontAwesomeIcon icon={faTimes}/></button></li>
              <li data-testid="openEditModal" onClick={() => context.openEditMovieModal(movie)}>Edit</li>
              <li data-testid="openDeleteModal" onClick={() => context.openDeleteMovieModal(movie.id)}>Delete</li>
            </ul>
          </nav>
      }
      
      <Link to={`/film/${movie.id}`}>
        <img src={imageUrl} alt={movie.title}/>
      </Link>
      <div className="row" id="img-details">
        <div className="col-10" id="title-detail">{movie.title}</div>
        <div className="col-2" id="date-detail">{movie.release_date}</div>
        <div className="col-12" id="genre-detail">
          {
            movie.genres.join(',')
          }
        </div>
      </div>
    </div>
  )
}
export default Movie;