import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
//import store from '../../store/store';
import { Provider } from "react-redux";
import { act } from 'react-dom/test-utils';
import ModalContext from "../modalContext";
import { shallow } from 'enzyme';

import Movie from './movie';
import { MovieMock } from '../mockData';
import userEvent from '@testing-library/user-event';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../store/initialState';
import { MemoryRouter, Route } from 'react-router';

const reactMock = require('react')
const mockStore = configureMockStore();

describe('Movie', () => {
   xit('launch submitMovie event', async () => {
      const initialState = {
        movieReducer: {
          loading: false
        }
      } 
      const store = mockStore(initialState);
      const updateMovie = jest.fn();
      render(
        <Movie updateMovie={updateMovie} movie={MovieMock}/>
      );
      await fireEvent.click(screen.getByText(/SAVE/i));
      screen.debug();
      expect(updateMovie).toHaveBeenCalledTimes(0);
   });

   it('launch openEditMovieModal event', () => {
    const initialStateDotMenu = false;
    React.useState = jest.fn().mockReturnValue([initialStateDotMenu, {}])
    const openEditMovieModal = jest.fn();
    render(
      <MemoryRouter initialEntries={['/film/1']}>
        <Route path="/film/1">
        <ModalContext.Provider value={{openEditMovieModal}}>
          <Movie movie={MovieMock}/>
        </ModalContext.Provider>
        </Route>
      </MemoryRouter>
      
    );
    fireEvent.click(screen.getByTestId('openEditModal'),{});
    expect(openEditMovieModal).toHaveBeenCalledTimes(1);
   });

   it('launch openDeleteMovieModal event', () => {
    const initialStateDotMenu = false;
    React.useState = jest.fn().mockReturnValue([initialStateDotMenu, {}])
    const openDeleteMovieModal = jest.fn();
    render(
      <MemoryRouter initialEntries={['/film/1']}>
        <Route path="/film/1">
        <ModalContext.Provider value={{openDeleteMovieModal}}>
          <Movie movie={MovieMock}/>
        </ModalContext.Provider>
        </Route>
      </MemoryRouter>
      
    );
    fireEvent.click(screen.getByTestId('openDeleteModal'),{});
    expect(openDeleteMovieModal).toHaveBeenCalledTimes(1);
   });

})