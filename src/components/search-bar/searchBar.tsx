import './searchBar.css';
import { Link } from "react-router-dom";
import { useState } from 'react';

const SearchBar = () => {
  const [title, setTitle] = useState("");

  const handleOnChangeTitle =(event:any) => {
    setTitle(event.target.value)
  }
  return (
    <>
      <h1 id="search-title">FIND YOUR MOVIE</h1>
      <input onChange={(e)=>handleOnChangeTitle(e)} id="search-input" type="text" placeholder="What do you want to watch?"></input>
      <Link id="search-button" className="reset-button" to={`/search?title=${title}`} >SEARCH</Link>
    </>
  )
}

export default SearchBar;