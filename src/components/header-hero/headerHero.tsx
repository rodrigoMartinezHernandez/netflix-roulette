import React from "react";
import SearchBar from "../search-bar/searchBar";

type HeaderHeroProps = {
  openAddMovieModal: () => void

}

const HeaderHero = ({openAddMovieModal}:HeaderHeroProps) => {
  return(
    <>
      <div className="col-6" id="btn-add">
        <button
          onClick={() => openAddMovieModal()}
        >+ADD MOVIE</button>
      </div>
      <div className="col-12 search-container">
        <SearchBar></SearchBar>
      </div>
    </>
  )
}
export default HeaderHero;
