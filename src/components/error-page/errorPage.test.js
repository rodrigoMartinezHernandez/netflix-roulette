import React from 'react';
import { MemoryRouter, Route } from 'react-router';
import { render } from '@testing-library/react';
import ErrorPage from './errorPage';

const renderComponent = () => 
  render(
    <MemoryRouter initialEntries={['/search']}>
      <Route path="/search">
        <ErrorPage />
      </Route>
    </MemoryRouter>
  );

it('renders component', () => {
  const { getByText } = renderComponent();

  expect(getByText(/Page Not Found/i)).toBeInTheDocument();
})