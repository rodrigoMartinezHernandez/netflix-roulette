import { Link } from "react-router-dom";
import './errorPage.css';

const ErrorPage = () => {
  return(
    <div className="error-page-container">
      <h1>Page Not Found</h1>
      <h1 className="code-error">404</h1>
      <Link className="back-home-button" to="/search" >GO BACK TO HOME</Link>
    </div>
  );
}

export default ErrorPage;