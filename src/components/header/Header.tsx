import React from "react";
import './Header.css';

const Header = (props:any) => {
  return(
      <header>
        <div id="filter">
          <div className="container">
            <div className="row">
              <div className="col-6" id="logo">
                <p><b>netflix</b>roulette</p>
              </div>
              {
                props.children
              }
            </div>
          </div>
        </div>
      </header>
  )
}

export default Header;