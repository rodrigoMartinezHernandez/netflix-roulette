import './errorBoundary.css';

const ErrorBoundary = (props:any) => {
  const OopsText = () => (
    <div className="container-error">
      <h2>
        Oops, something went wrong... We are doing our bestto fix the issue.
      </h2>
    </div>
  )

  let isEverythingOk = true;

  return <>{isEverythingOk?props.children:<OopsText/>}</>
}

export default ErrorBoundary;