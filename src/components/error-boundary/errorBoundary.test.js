import { render } from '@testing-library/react';

import ErrorBoundary from './errorBoundary';

describe('ErrorBoundary', () => {
  test('renders ErrorBoundary snapshot', () => {
    const { asFragment } = render(<ErrorBoundary />);

    expect(asFragment(<ErrorBoundary />)).toMatchSnapshot();
  });
    

})