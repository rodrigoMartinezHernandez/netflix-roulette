import react from 'react';
import { render } from '@testing-library/react';

import Footer from './footer';

describe('Footer', () => {
  test('renders Footer snapshot', () => {
    const { asFragment } = render(<Footer />);

    expect(asFragment(<Footer />)).toMatchSnapshot();
  });
    

})