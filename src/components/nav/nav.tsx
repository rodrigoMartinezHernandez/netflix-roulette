import { useState } from 'react';
import { connect } from 'react-redux';
import { fetchMoviesDispatch } from '../../store/movieDispatchers';
import './nav.css';

const Nav = ({ fetchMovies }:any) => {
  const [sortFilter, setSortFilter] = useState("");
  const [searchFilter, setSearchFilter] = useState("");
  
  const handleSort = (e:any) => {
    const filter = "sortBy="+e.target.value+"&sortOrder=desc";
    setSortFilter(filter);
    fetchMovies(getFilters(filter, searchFilter));

  }
  const handleSearch = (genre:string) => {
    const filter = "searchBy=genres&search="+genre;
    setSearchFilter(filter);
    fetchMovies(getFilters(sortFilter, filter));

  }

  const getFilters = (sortFilter:string, searchFilter:string) => {
    return '?'+sortFilter+'&'+searchFilter;
  }
 
  return(
    <nav id="filter-nav">
      <ul id="filter-ul">
        <li role="button" onClick={()=>handleSearch("")}>ALL</li>
        <li role="button" onClick={()=>handleSearch("documentary")}>DOCUMENTARY</li>
        <li role="button" onClick={()=>handleSearch("comedy")}>COMEDY</li>
        <li role="button" onClick={()=>handleSearch("horror")}>HORROR</li>
        <li role="button" onClick={()=>handleSearch("crime")}>CRIME</li>
        <form action="" id="nav-form">
          <label htmlFor="sort-by">SORT BY</label>
          <select name="sort-by" id="filter-sort" onChange={(e) => handleSort(e)}>
            <option value="">choose one</option>
            <option value="release_date">RELEASE DATE</option>
            <option value="vote_average">RATING</option>
          </select>
        </form>
      </ul>
    </nav>
  )
}
const mapDispatchToProps = {
  fetchMovies: (filters: any) => fetchMoviesDispatch(filters)
}

export default connect(null, mapDispatchToProps)(Nav);