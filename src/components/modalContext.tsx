import React from 'react';
import { MovieType } from '../interfaces/movie';
type ContextType = {
  closeAddMovieModal: () => void,
  openMovieDetails: (movie: MovieType) => void,
  closeDeleteMovieModal: () => void,
  closeEditMovieModal: () => void,
  openEditMovieModal: (movie: MovieType) => void,
  openDeleteMovieModal: (id: string) => void,
  closeMovieDetails: () => void,
}
const ModalContext = React.createContext<ContextType>({
  closeAddMovieModal: () => {},
  openMovieDetails: () => {},
  closeDeleteMovieModal: () => {},
  closeEditMovieModal: () => {},
  openEditMovieModal: () => {},
  openDeleteMovieModal: () => {},
  closeMovieDetails: () => {}
});

export default ModalContext;