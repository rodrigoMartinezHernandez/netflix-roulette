import { useEffect } from "react";
import { connect } from 'react-redux';
import { MovieType, StateType } from "../../interfaces/movie";
import { fetchMoviesDispatch } from "../../store/movieDispatchers";
import Movie from "../movie/movie";
import Nav from "../nav/nav";
import './moviesList.css';

import { useLocation } from "react-router-dom";
import NoMoviesFound from "../no-movies-found/noMoviesFound";


const useQuery = () => {
  return new URLSearchParams(useLocation().search);
}

const MoviesList = ({fetchMovies, movies, totalAmount}: any) => {
  let query = useQuery();
  var title = query.get("title") || "";

  useEffect(() => {
    fetchMovies(`?searchBy=title&search=${title}`);
  }, [fetchMovies, title]);
  
  

  
  console.log("Movies: ", totalAmount);
  const validateMoviesLoad = () => {
    return !!title && movies?.length !== 0 && movies !== undefined;
  }

  

  return(
      <div className="container-movielist">
        <div className="container">
          <Nav></Nav>
          <h4 className="number-results">{totalAmount} movies found</h4>
          <div className="row">
            {
              validateMoviesLoad()
                ? movies.map((movie: MovieType) => 
                  <Movie 
                    key={movie.id} 
                    movie={movie} 
                  ></Movie>
                )
                : <NoMoviesFound></NoMoviesFound>
            }
          </div>
        </div>
      </div>
  )
}

const mapStateToProps = (state: StateType) =>({ 
  movies: state.movieReducer.movies.data,
  loading: state.movieReducer.loading,
  totalAmount: state.movieReducer.movies.totalAmount
});


const mapDispatchToProps = {
    fetchMovies: (filters: any) => fetchMoviesDispatch(filters)
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviesList);